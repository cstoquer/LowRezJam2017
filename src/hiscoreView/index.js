var viewManager = require('../viewManager');
var leaderboardManager = require('../LeaderboardManager');

var CHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ';
var CHAR_LEN = CHARS.length;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function getTop(cb) {
	leaderboardManager.GetTopLeaderboard(cb);
	// window.setTimeout(function () {
	// 	var top = [
	// 		{ name: 'cedric', score: 100000 },
	// 		{ name: 'manu',   score:    100 },
	// 		{ name: 'trisk',  score:     50 },
	// 		{ name: 'trisk',  score:     30 },
	// 		{ name: 'trisk',  score:     20 },
	// 		{ name: 'trisk',  score:     10 },
	// 		{ name: 'trisk',  score:      0 }
	// 	];
	// 	cb(null, top);
	// }, 2000);
}

function setUserScore(name, score) {
	leaderboardManager.SaveUserScore(name, score, function (error) {
		// TODO
	});
	// console.log('setUserScore', name, score);
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
var cursor = { x: 0, y: 0, chr: 0 };
var name   = ['A', '', '', '', '', ''];
var frame  = 0;
var ready  = false;
var userScore = 0;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
exports.open = function (game) {
	ready = false;
	userScore = game.score;
	camera(1, 1);
	var background = getMap('hiscore');
	background.redraw(); // switch to current palette
	for (var i = 0; i < 8; i++) {
		getMap('spinner/' + i).redraw();
	}
	draw(background);
	pen(1);

	getTop(function (error, top) {
		console.log('>>>>', top)
		ready = true;
		draw(getMap('spinner/off'), 27, 28);
		if (error) {
			// TODO: display error
			return;
		}

		// cursor.x = 0;
		cursor.y = 6;
		var haveHiScore = 0;

		for (var i = 0; i < 7; i++) {
			var player = top[i - haveHiScore];
			if (!player) player = { userName: 'trisk', score: 0 };

			if (!haveHiScore && game.score >= player.score) {
				// set user score inside leaderboard
				cursor.y = i;
				haveHiScore = 1;
				player = { userName: name.join(''), score: game.score };
			}

			var pname = player.userName.substr(0, 6);
			var score = ('000000' + ~~(player.score)).substr(-6);
			print(pname,  6, 7 * i + 13);
			print(score, 37, 7 * i + 13);
		}
	});
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function printChar(back) {
	var s = back ? 194 : 195;
	sprite(s, cursor.x * 4 + 5, cursor.y * 7 + 12);
	sprite(s, cursor.x * 4 + 5, cursor.y * 7 + 13);
	print(name[cursor.x], cursor.x * 4 + 6, cursor.y * 7 + 13);
}

function shiftChar(dir) {
	printChar(false);
	cursor.x += dir;
	if (name[cursor.x]) {
		cursor.chr = CHARS.indexOf(name[cursor.x]);
	} else {
		name[cursor.x] = CHARS[cursor.chr];
	}
}

exports.update = function (gamepad) {
	if (!ready) {
		frame = (frame + 1) % 32;
		draw(getMap('spinner/' + ~~(frame / 4)), 27, 28);
		return;
	}

	if ((btnp.right || gamepad.btnp.right) && cursor.x < 5) shiftChar( 1);
	if ((btnp.left  || gamepad.btnp.left ) && cursor.x > 0) shiftChar(-1);

	if (btnp.up || gamepad.btnp.up) {
		cursor.chr = (cursor.chr + 1) % CHAR_LEN;
		name[cursor.x] = CHARS[cursor.chr];
	}

	if (btnp.down || gamepad.btnp.down) {
		cursor.chr -= 1;
		if (cursor.chr < 0) cursor.chr = CHAR_LEN - 1;
		name[cursor.x] = CHARS[cursor.chr];
	}

	frame = (frame + 1) % 10;
	printChar(frame > 5);
	
	if (btnp.A || btnp.B || gamepad.btnp.A || gamepad.btnp.B) {
		// send result
		setUserScore(name.join(''), userScore);
		viewManager.open('game', 0);
	}
};