var API_KEY    = "109e4e3b9d442b31211b243a5fe3a66f0f832dd54c03e03730d054737523427b";
var SECRET_KEY = "82317ba2a0abecdbfc623da8d786bc480acdc47847b0a9338194fb824e323146";
var GAME_NAME  = "Trisk";

function LeaderboardManager() {
	App42.initialize(API_KEY, SECRET_KEY);

	this.scoreBoardService = new App42ScoreBoard();

	this.gameName = GAME_NAME;
	this.max = 7;
}

LeaderboardManager.prototype.SaveUserScore = function(userName, score, cb) {
	this.scoreBoardService.saveUserScore(this.gameName, userName, score,{
		success: function () {
			cb(null);
		},
		error: function (error) {
			cb(error);
		}
	});
};

LeaderboardManager.prototype.GetTopLeaderboard = function(cb) {
	this.scoreBoardService.getTopNRankers(this.gameName, this.max,{
		success: function (object) {
			var players = [];
			var game = JSON.parse(object);
			var result = game.app42.response.games.game;
			var scoreList = result.scores.score;

			// scoreList may be only an object and not an array of length 1...
			if (!(scoreList instanceof Array)) {
				scoreList = [].concat(scoreList);
			}

			for (var i = 0; i < scoreList.length; i++) {
				var scoreItem = scoreList[i];
				var player = {
					userName: scoreItem.userName,
					score: scoreItem.value
				};
				players.push(player);
			}

			cb(null, players);
		},
		error: function (error) {
			// First to enter the leaderboard ... network...
			cb(error);
		}
	});
};

LeaderboardManager.prototype.IsHighScore = function(score, cb) {
	this.GetTopLeaderboard(function(error, players) {
		cb(error, players[players.length - 1].score < score);
	});
};

module.exports = new LeaderboardManager();

// function DoTest() {
// 	var leaderboardManager = new LeaderboardManager();
// 	leaderboardManager.SaveUserScore("Cain", 1, function(error) {
// 		 console.log("SaveUserScore " + error);
// 		 leaderboardManager.GetTopLeaderboard(function(error, players) {
// 			  console.log("GetTopLeaderboard");
// 			  console.log(error);
// 			  console.log(players);
// 		 });
// 	});
// }

// DoTest();

