var viewManager = require('../viewManager');

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function waitAnimation(gamepad) {
	if (btnp.A || btnp.B || gamepad.btnp.A || gamepad.btnp.B) viewManager.open('game', 0);
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
var frame = 0;
var ctx = $screen.ctx;
var ANIMATION_LENGTH = 60;
var LOGO = getMap('logo');
var pivot = 28;

function splashAnimation(gamepad) {
	frame += 1;
	ctx.globalAlpha = 0.15;
	cls();
	ctx.globalAlpha = 1;

	var scaleSin = Math.sin(frame / 20);
	var posCos   = Math.cos(frame / 20);
	var scale    = 0.8 + scaleSin * 0.2;
	// var scale    = 0.5 + 0.5 * frame / ANIMATION_LENGTH;

	ctx.save();
	ctx.translate(4 + pivot, 0);
	// ctx.scale(scale, scale);
	ctx.translate(-pivot, 0)
	draw(LOGO, 0, 20 - 10 * posCos);
	ctx.restore();
	// exports.update = waitAnimation;


	if (btnp.A || btnp.B || gamepad.btnp.A || gamepad.btnp.B) viewManager.open('game', 0);

}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
exports.open = function (params) {
	camera(0, 0);
	frame = 0;
	paper(0);
	cls();
};

exports.update = splashAnimation;