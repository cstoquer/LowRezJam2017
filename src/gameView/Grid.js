var constants             = require('../constants');
var EventEmitter          = require('EventEmitter');
var TileMap               = require('TileMap');
var DestroyTilesAnimation = require('./animations/DestroyTilesAnimation');
var ComboChainAnimation   = require('./animations/ComboChainAnimation');
var blink                 = getMap('blink');

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function Grid() {
	EventEmitter.call(this);

	this.nTileType  = 5;
	this.map        = new TileMap(6, 10); //getMap('grid').copy();
	this.nextLine   = new TileMap(6, 1);  //getMap('newLine').copy();
	this.animations = []; // tile falling animations, tile destroying animations
	this.blink      = 0;  // blink animation
}
inherits(Grid, EventEmitter);
module.exports = Grid;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Grid.prototype.reset = function () {
	// this.map        = getMap('grid').copy(); // TODO: randomize
	// this.nextLine   = getMap('newLine').copy(); // TODO: randomize
	this.animations = []; // tile falling animations, tile destroying animations
	this.randomizeGrid();
	this.randomizeNextLine();
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Grid.prototype.randomizeNextLine = function () {
	for (var x = 0; x < this.map.width; x++) {
		this.nextLine.set(x, 0, random(this.nTileType) + constants.DARK_TILE_OFFSET + 1);
	}
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Grid.prototype.randomizeGrid = function () {
	this.map.clear();
	for (var x = 0; x < this.map.width; x++) {
		for (var y = 0; y < 7; y++) {
			this.map.set(x, y, 0);
		}
		for (var y = 7; y < this.map.height; y++) {
			var disallow = {};
			// nota: map return null if we try to get a tile outside of bounds
			var top    = this.map.get(x, y - 1);
			var bottom = this.map.get(x, y + 1);
			var left   = this.map.get(x - 1, y);
			var right  = this.map.get(x + 1, y);

			if (top    && top.sprite   ) disallow[top.sprite]    = true;
			if (bottom && bottom.sprite) disallow[bottom.sprite] = true;
			if (left   && left.sprite  ) disallow[left.sprite]   = true;
			if (right  && right.sprite ) disallow[right.sprite]  = true;


			var n = Object.keys(disallow).length;
			var rand = random(this.nTileType - n);
			var sprite = 1;

			while (rand > 0 || disallow[sprite]) {
				if (!disallow[sprite]) rand -= 1;
				sprite += 1;
			}
			this.map.set(x, y, sprite);
		}
	}
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Grid.prototype.render = function (scroll) {
	draw(this.map, 0, -scroll);
	draw(this.nextLine, 0, constants.GRID_HEIGHT * constants.TILE_SIZE - scroll);

	this.blink = (this.blink + 1) % 12;
	if (this.blink === 0) {
		var blinked = false;
		for (var x = 0; x < this.map.width; x++) {
			for (var y = 0; y < 2; y++) {
				if (this.map.get(x, y).sprite) {
					draw(blink, x * constants.TILE_SIZE, y * constants.TILE_SIZE - scroll);
					blinked = true;
					continue;
				}
			}
		}
		if (blinked) sfx('blink', 0.7);
	}

	for (var i = this.animations.length - 1; i >= 0; i--) {
		this.animations[i].update(scroll);
	}
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Grid.prototype.addAnimation = function (animation) {
	this.animations.unshift(animation);
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Grid.prototype.removeAnimation = function (animation) {
	var index = this.animations.indexOf(animation);
	if (index === -1) return console.error('animation not in list');
	this.animations.splice(index, 1);
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
/** Destroy all tile that have been marked as `_toBeDestroyed` */
Grid.prototype.destroy = function (combo, chain, cb) {
	var map = this.map;
	var destroyAnimation = new DestroyTilesAnimation(this);

	for (var x = 0; x < map.width;  x++) {
		for (var y = 0; y < map.height; y++) {
			var tile = map.get(x, y);
			if (tile._toBeDestroyed) {
				destroyAnimation.add(x, y, tile.sprite);
			}
		}
	}

	this.addAnimation(destroyAnimation);

	var self = this;

	// emit before to pause the game during the animation
	var nDestroyed = combo.tiles;
	this.emit('destroyed', nDestroyed, chain);

	destroyAnimation.on('end', function onEnd() {
		self.removeAnimation(destroyAnimation);
		for (var x = 0; x < map.width;  x++) {
			for (var y = 0; y < map.height; y++) {
				var tile = map.get(x, y);
				if (tile._toBeDestroyed) {
					map.set(x, y, 0);
				}
			}
		}
		cb(true);
	});
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Grid.prototype.markLineToDestroy = function (x1, x2, y) {
	while (x1 < x2) {
		var cell = this.map.get(x1++, y);
		cell._toBeDestroyed = true; // mark tile to be destroyed
		cell._locked = true; // tile can not be moved by cursor
	}
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Grid.prototype.markColumnToDestroy = function (x, y1, y2) {
	while (y1 < y2) {
		var cell = this.map.get(x, y1++);
		cell._toBeDestroyed = true; // mark tile to be destroyed
		cell._locked = true; // tile can not be moved by cursor
	}
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Grid.prototype.checkLines = function () {
	var lines = 0;
	var tiles = 0;
	for (var y = 0; y < this.map.height; y++) {
		var current = this.map.get(0, y);
		current = current && current.sprite;
		var start = 0;
		for (var x = 1; x < this.map.width; x++) {
			var tile = this.map.get(x, y).sprite;
			if (tile === current) continue;
			if (current && x - start >= 3) {
				lines += 1;
				tiles += x - start;
				this.markLineToDestroy(start, x, y);
			}
			start = x;
			current = tile;
		}
		if (current && x - start >= 3) {
			lines += 1;
			tiles += x - start;
			this.markLineToDestroy(start, x, y);
		}
	}
	return { lines: lines, tiles: tiles };
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Grid.prototype.checkColumns = function () {
	var lines = 0;
	var tiles = 0;
	for (var x = 0; x < this.map.width; x++) {
		var current = this.map.get(x, 0);
		current = current && current.sprite;
		var start = 0;
		for (var y = 1; y < this.map.height; y++) {
			var tile = this.map.get(x, y).sprite;
			if (tile === current) continue;
			if (current && y - start >= 3) {
				lines += 1;
				tiles += y - start;
				this.markColumnToDestroy(x, start, y);
			}
			start = y;
			current = tile;
		}
		if (current && y - start >= 3) {
			lines += 1;
			tiles += y - start;
			this.markColumnToDestroy(x, start, y);
		}
	}
	return { lines: lines, tiles: tiles };

};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Grid.prototype.makeTileFall = function (cb) {
	var changed = false;
	for (var x = 0; x < this.map.width; x++) {
		var lastEmpty = null;
		for (var y = this.map.height - 1; y >= 0; y--) {
			// sprite 0 is empty
			var tile   = this.map.get(x, y);
			var sprite = tile.sprite;
			if (tile._locked || sprite === constants.SWITCHING_SPRITE) {
				// tile cannot fall
				lastEmpty = null;
			} else if (sprite && lastEmpty) {
				// make this tile fall into last empty
				this.map.set(x, y, 0);
				this.map.set(x, lastEmpty, sprite);
				changed = true;
				// increment empty slot
				lastEmpty -= 1;
			} else if (!sprite && !lastEmpty) {
				lastEmpty = y;
			}
		}
	}
	if (changed) sfx('fall');
	return cb(changed);
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Grid.prototype._checkGrid = function (comboChain, cb) {
	var self = this;

	this.makeTileFall(function (fallen) {
		// TODO: fall animation
		var comboLine    = self.checkLines();
		var comboColumns = self.checkColumns();

		var tiles = comboLine.tiles + comboColumns.tiles;
		var lines = comboLine.lines + comboColumns.lines;

		// call destroy only if needed
		if (!lines) return cb(fallen);

		var combo = { lines: lines, tiles: tiles };
		comboChain.push(combo);

		var chain = comboChain.length;
		if (chain > 1) {
			// TODO: choose different location
			var chainAnimation = new ComboChainAnimation(chain, 7, 26);
			self.addAnimation(chainAnimation);
			chainAnimation.on('end', function () {
				self.removeAnimation(chainAnimation);
			});
		}

		self.destroy(combo, comboChain, function (destroyed) {
			// TODO: combo animation
			return cb(destroyed || fallen);
		});
	});
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Grid.prototype._checkRecurse = function (comboChain) {
	var self = this;
	this._checkGrid(comboChain, function (changed) {
		if (!changed) {
			// console.log(comboChain);
			return;
		}
		self._checkRecurse(comboChain);
	});
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Grid.prototype.checkRecurse = function () {
	var comboChain = [];
	this._checkRecurse(comboChain);
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Grid.prototype.addNewLine = function () {
	// shift the grid up
	this.map.paste(this.map, 0, -1);

	// copy new line at the bottom and generate another random new line
	var y = this.map.height - 1;
	for (var x = 0; x < this.map.width; x++) {
		var tile = this.nextLine.get(x, 0).sprite - constants.DARK_TILE_OFFSET;
		this.map.set(x, y, tile);
		this.nextLine.set(x, 0, random(this.nTileType) + constants.DARK_TILE_OFFSET + 1);
	}

	// check if combo occurs with the new line appearing
	this.checkRecurse();
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Grid.prototype.isGameOver = function () {
	// check line at the top => GAME OVER
	for (var x = 0; x < this.map.width; x++) {
		if (this.map.get(x, 0).sprite) return true;
	}
	return false;
};
