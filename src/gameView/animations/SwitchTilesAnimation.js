var EventEmitter = require('EventEmitter');
var TILE_WIDTH  = settings.tileSize.width;
var TILE_HEIGHT = settings.tileSize.height;
var ANIM_LENGTH = 6;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function SwitchTilesAnimation(x, y, spriteA, spriteB) {
	EventEmitter.call(this);
	this.x = x;
	this.y = y;
	this.spriteA = spriteA;
	this.spriteB = spriteB;
	this.frame = 0;
}
inherits(SwitchTilesAnimation, EventEmitter);
module.exports = SwitchTilesAnimation;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
SwitchTilesAnimation.prototype.update = function (scroll) {
	this.frame += 1;
	if (this.frame >= ANIM_LENGTH) {
		this.emit('end');
		this.frame = ANIM_LENGTH;
	}
	sprite(this.spriteA, this.x * TILE_WIDTH + this.frame,              this.y * TILE_HEIGHT - scroll);
	sprite(this.spriteB, this.x * TILE_WIDTH + TILE_WIDTH - this.frame, this.y * TILE_HEIGHT - scroll);
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
SwitchTilesAnimation.prototype.scroll = function () {
	if (this.y <= 0) return this.emit('end');
	this.y -= 1;
}