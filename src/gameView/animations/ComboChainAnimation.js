var EventEmitter = require('EventEmitter');
var Texture      = require('Texture');
var ANIM_LENGTH  = 30;
var Y_SPEED      = -1/3;


// prepare template texture
var TEMPLATE = new Texture(20, 13);

function drawTextOutline(texture, text, x, y) {
	texture.pen(0);
	texture.print(text, x - 1, y - 1);
	texture.print(text, x - 1, y    );
	texture.print(text, x - 1, y + 1);
	
	texture.print(text, x    , y - 1);
	texture.print(text, x    , y + 1);

	texture.print(text, x + 1, y - 1);
	texture.print(text, x + 1, y    );
	texture.print(text, x + 1, y + 1);

	texture.pen(1);
	texture.print(text, x, y);
}

drawTextOutline(TEMPLATE, 'chain', 1, 1);
TEMPLATE.sprite(223, 5, 7);

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function ComboChainAnimation(nChain, x, y) {
	EventEmitter.call(this);
	this.x = x;
	this.y = y;
	this.frame = 0;
	this.texture = new Texture(20, 13);
	this._prepare(nChain);
}
inherits(ComboChainAnimation, EventEmitter);
module.exports = ComboChainAnimation;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
ComboChainAnimation.prototype._prepare = function (nChain) {
	this.texture.draw(TEMPLATE, 0, 0);
	drawTextOutline(this.texture, nChain, 11, 7);
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
ComboChainAnimation.prototype.update = function (scroll) {
	this.frame += 1;
	this.y += Y_SPEED;
	if (this.frame >= ANIM_LENGTH) {
		this.emit('end');
		this.frame = ANIM_LENGTH;
	}
	if (~~(this.frame) % 3 !== 0) {
		draw(this.texture, this.x, this.y);
	}
};
