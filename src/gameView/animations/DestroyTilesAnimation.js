var EventEmitter = require('EventEmitter');
var TileMap      = require('TileMap');
var TILE_WIDTH  = settings.tileSize.width;
var TILE_HEIGHT = settings.tileSize.height;
var ANIM_LENGTH = 11 * 2;

var TILE_OFFSET = 48;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function DestroyTilesAnimation(grid) {
	EventEmitter.call(this);
	this.frame = 0;
	this.map   = new TileMap(grid.map.width, grid.map.height);
	this.cells = [];

	// this._init(grid);
}
inherits(DestroyTilesAnimation, EventEmitter);
module.exports = DestroyTilesAnimation;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
// DestroyTilesAnimation.prototype._init = function (grid) {
// 	var map = grid.map;
// 	for (var x = 0; x < map.width; x++) {
// 		for (var y = 0; y < map.height; y++) {
// 			var tile = map.get(x, y);
// 			if (tile._toBeDestroyed) {
// 				this.map.set(x, y, tile.sprite + TILE_OFFSET);
// 			}
// 		}
// 	}
// };

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
DestroyTilesAnimation.prototype.add = function (x, y, sprite) {
	// this.map.set(x, y, sprite + TILE_OFFSET);
	this.map.set(x, y, 68);
	var tile = this.map.get(x, y);
	this.cells.push(tile);
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
DestroyTilesAnimation.prototype.getDuration = function (nDestroy) {
	// TODO
	return 22;
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
DestroyTilesAnimation.prototype.redraw = function () {
	var sprite = 68 + ~~(this.frame / 2);
	for (var i = 0; i < this.cells.length; i++) {
		var tile = this.cells[i];
		this.map.set(tile.x, tile.y, sprite);
	}
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
DestroyTilesAnimation.prototype.update = function (scroll) {
	this.frame += 1;
	if (this.frame >= ANIM_LENGTH) {
		this.emit('end');
		this.frame = ANIM_LENGTH;
	}
	
	// animation
	if (this.frame % 2 === 0) {
		this.redraw();
	}

	draw(this.map, 0, -scroll);

};
