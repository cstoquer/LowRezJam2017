
//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function Stage(pieces, speed, lines) {
	this.pieces = pieces; // number of pieces type that spawn (max 7)
	this.speed  = speed;  // scrolling speed (in pixel per frame)
	this.lines  = lines;  // lines required to go to next
	// this.color  = color;  // color index in palettes
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
var stages = [
	// (pieces, speed, next)
	new Stage(4, 0.01,  5),
	new Stage(5, 0.01,  5),
	new Stage(5, 0.02,  5),
	new Stage(5, 0.03, 10),
	new Stage(5, 0.04, 10),
	new Stage(5, 0.04, 10),
	new Stage(5, 0.05, 10),
	new Stage(5, 0.06, 10),
	new Stage(6, 0.05, 10),
	new Stage(6, 0.06, 10),
	new Stage(6, 0.07, 20),
	new Stage(6, 0.08, 20),
	new Stage(6, 0.09, 20),
	new Stage(6, 0.10, 20),
	new Stage(6, 0.11, 20),
	new Stage(6, 0.12, 20),
	new Stage(6, 0.13, 20),
	new Stage(6, 0.14, 20),
	new Stage(6, 0.15, 20),
	new Stage(6, 0.16, 20),
	new Stage(6, 0.17, 25),
	new Stage(6, 0.18, 25),
	new Stage(6, 0.19, 25),
	new Stage(6, 0.20, 25),
	new Stage(6, 0.21, 25),
	new Stage(6, 0.22, 25),
	new Stage(6, 0.23, 25),
	new Stage(6, 0.24, 25),
	new Stage(6, 0.25, 25),
	new Stage(6, 0.26, 30),
	new Stage(6, 0.27, 30),
	new Stage(6, 0.28, 30),
	new Stage(6, 0.29, 30),
	new Stage(6, 0.30, 30),
	new Stage(6, 0.31, 30),
	new Stage(6, 0.32, 30),
	new Stage(6, 0.33, 30),
	new Stage(6, 0.34, 30),
	new Stage(6, 0.35, 30),
	new Stage(6, 0.36, 30),
	new Stage(6, 0.37, 30),
	new Stage(6, 0.38, 30),
	new Stage(6, 0.39, 30),
	new Stage(7, 0.30, 30),
	new Stage(7, 0.31, 30),
	new Stage(7, 0.32, 30),
	new Stage(7, 0.33, 30),
	new Stage(7, 0.34, 30),
	new Stage(7, 0.35, 30),
	new Stage(7, 0.36, 30),
	new Stage(7, 0.37, 30),
	new Stage(7, 0.38, 30),
	new Stage(7, 0.39, 30),
	new Stage(7, 0.40, 40),
	new Stage(7, 0.41, 40),
	new Stage(7, 0.42, 40),
	new Stage(7, 0.43, 40),
	new Stage(7, 0.44, 40),
	new Stage(7, 0.45, 40),
	new Stage(7, 0.46, 40),
	new Stage(7, 0.47, 40),
	new Stage(7, 0.48, 40),
	new Stage(7, 0.49, 40),
	new Stage(7, 0.50, 40),
];

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
exports.get = function (stageIndex) {
	if (stageIndex >= stages.length - 1) {
		stageIndex = stages.length - 1
	}
	return stages[stageIndex];
};