var backgroundMap = getMap('stage');
var texture = backgroundMap.texture;
var BIG_NUMBERS = [
	getMap('number/0'),
	getMap('number/1'),
	getMap('number/2'),
	getMap('number/3'),
	getMap('number/4'),
	getMap('number/5'),
	getMap('number/6'),
	getMap('number/7'),
	getMap('number/8'),
	getMap('number/9')
];

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function prepare() {
	backgroundMap.redraw();
	// progress bar
	texture.pen(1);
	texture.rect(40, 1, 23, 6);

	// stage
	texture.pen(0).paper(6);
	texture.rectfill(40, 9, 23, 7);
	texture.print('stage', 42, 10);

	// next
	texture.rectfill(40, 32, 23, 7);
	texture.print('next', 44, 33);

	//score
	texture.rectfill(40, 48, 23, 7);
	texture.print('score', 42, 49);
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
exports.draw = function () {
	draw(texture, -2, -4);
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
exports.setStage = function (stage) {
	prepare();
	if (stage > 99) {
		stage = stage.toString();
		texture.draw(BIG_NUMBERS[stage[0]], 39, 18);
		texture.draw(BIG_NUMBERS[stage[1]], 46, 18);
		texture.draw(BIG_NUMBERS[stage[2]], 53, 18);
		return;
	}
	stage = (stage < 10 ? '0' : '') + stage.toString();
	texture.draw(BIG_NUMBERS[stage[0]], 42, 18);
	texture.draw(BIG_NUMBERS[stage[1]], 49, 18);
};