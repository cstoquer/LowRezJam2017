var constants           = require('../constants');
var SwitchTileAnimation = require('./animations/switchTilesAnimation');


var cursorAsset = getMap('cursor');
var MAX_X = constants.GRID_WIDTH  - 2;
var MAX_Y = constants.GRID_HEIGHT - 1;
var CURSOR_OFFSET = 3;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function Cursor() {
	this.x = 2;
	this.y = 7;
	this.switching = null;
}

module.exports = Cursor;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Cursor.prototype.reset = function () {
	this.x = 2;
	this.y = 7;
	this.switching = null;
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Cursor.prototype.render = function (scroll) {
	if (this.switching) {
		this.switching.update(scroll);
	}
	draw(
		cursorAsset,
		this.x * constants.TILE_SIZE - CURSOR_OFFSET,
		this.y * constants.TILE_SIZE - CURSOR_OFFSET - scroll
	);
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Cursor.prototype.update = function (gamepad) {
	if ((btnp.left  || gamepad.btnp.left ) && this.x > 0)     { this.x -= 1; sfx('cursor'); }
	if ((btnp.right || gamepad.btnp.right) && this.x < MAX_X) { this.x += 1; sfx('cursor'); }
	if ((btnp.up    || gamepad.btnp.up   ) && this.y > 0)     { this.y -= 1; sfx('cursor'); }
	if ((btnp.down  || gamepad.btnp.down ) && this.y < MAX_Y) { this.y += 1; sfx('cursor'); }
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Cursor.prototype.scroll = function () {
	if (this.y > 0) this.y -= 1;
	if (this.switching) this.switching.scroll();
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Cursor.prototype.switchTiles = function (grid) {
	if (this.switching) return;

	var x = this.x;
	var y = this.y;

	// get the two cells to be switched
	var cellA = grid.map.get(x,     y);
	var cellB = grid.map.get(x + 1, y);

	// if both cells are empty, do nothing
	if (cellA.sprite === 0 && cellB.sprite === 0) return;

	// check that one cell is not locked or switching
	if (cellA._locked || cellB._locked) return;
	if (cellA.sprite === constants.SWITCHING_SPRITE) return;
	if (cellB.sprite === constants.SWITCHING_SPRITE) return;


	// set placeholder tile in the grid for the duration of the animation
	grid.map.set(x,     y, constants.SWITCHING_SPRITE);
	grid.map.set(x + 1, y, constants.SWITCHING_SPRITE);

	// start switch animation
	sfx('swap');
	var self = this;
	this.switching = new SwitchTileAnimation(x, y, cellA.sprite, cellB.sprite);
	this.switching.on('end', function onEnd() {
		// switch cells with real values
		grid.map.set(this.x,     this.y, cellB.sprite);
		grid.map.set(this.x + 1, this.y, cellA.sprite);

		// unlock
		self.switching = null;

		// check combos
		grid.checkRecurse();
	});
};