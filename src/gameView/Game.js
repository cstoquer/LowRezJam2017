var Texture         = require('Texture');
var Grid            = require('./Grid');
var Cursor          = require('./Cursor');
var stages          = require('./stages');
var viewManager     = require('../viewManager');
var color           = require('../color');
var constants       = require('../constants');
var background      = require('./background');
var backgroundGrid  = getMap('background');

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function Game() {
	this.grid     = new Grid();
	this.cursor   = new Cursor();
	this.speed    = 0.05;
	this.scroll   = 0;
	this.pause    = 0;
	this.maxPause = 0;
	this.stage    = 0;
	this.score    = 0;
	this.line     = 0; // curent line
	this.lines    = 9; // how many lines to do to go to next stage

	this._setEvents();
}

module.exports = Game;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Game.prototype.open = function (stageId) {
	stageId = stageId || 0;
	camera(-2, -4); // FIXME
	paper(1);
	this.score  = 0;
	this.pause  = 0;
	this.scroll = 0;
	this.setStage(stageId);
	this.cursor.reset();
	this.grid.reset();
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Game.prototype.setStage = function (stageId) {
	stageId = stageId || 0;
	var stage = stages.get(stageId);

	this.stage = stageId;
	this.lines = stage.lines;
	this.line  = 0;
	this.speed = stage.speed;
	this.next  = '00/' + (this.lines < 10 ? '0' : '') + this.lines;
	background.setStage(stageId + 1);

	this.changeColors(stageId % constants.MAX_PALETTE);
	this.grid.nTileType = stage.pieces;
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Game.prototype.update = function (gamepad) {
	if (this.pause > 0) {
		this.pause -= 1;
		if (btn.B || gamepad.btn.X) this.pause = 0;
	} else {
		var speed = this.speed;
		if (btn.B || gamepad.btn.X) speed = 1;
		this.scroll += speed;
		if (this.scroll >= constants.TILE_SIZE) {
			// check for gameover
			if (this.grid.isGameOver()) {
				viewManager.open('game over', this);
				return;
			}

			// scroll
			this.scroll -= constants.TILE_SIZE;
			this.grid.addNewLine();

			// increment line counter and check if next stage
			this.line += 1;
			this.next = (this.line < 10 ? '0' : '') + this.line + '/' + (this.lines < 10 ? '0' : '') + this.lines;

			if (this.line >= this.lines) {
				this.setStage(this.stage + 1);
			}

			// shift cursor with the grid
			this.cursor.scroll();
		}
	}

	this.render();

	if (btnp.A || gamepad.btnp.A || gamepad.btnp.lb) this.cursor.switchTiles(this.grid);
	// if (btnp.B || gamepad.btnp.B) this.changeColors(random(PALETTES.length / 3)); // TEST
	// if (btnp.B || gamepad.btnp.B) this.grid.randomizeGrid(); // TEST
	// if (btnp.B || gamepad.btnp.B) this.setStage(this.stage + 1); // TEST
	this.cursor.update(gamepad);
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Game.prototype.render = function () {
	background.draw();
	backgroundGrid.draw(0, -this.scroll - constants.TILE_SIZE);
	this.grid.render(this.scroll);
	this.cursor.render(this.scroll);
	print(this.next,  40, 37);
	// TODO: if score > 999999
	print(('00000' + this.score).substr(-6), 38, 53);
	if (this.pause > 0) {
		var width = 19 * this.pause / this.maxPause;
		rectfill(40, -1, width, 2);
	}
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
var originalPalette = new Texture(4, 2).sprite(226);
var COLOR_SOURCE_MAP = color.getPaletteMapFromImage(originalPalette.canvas);
var PALETTES = color.getColorsFromImage(assets.palettes);

function luminance(color, coef) {
	return {
		r: Math.min(255, Math.round(color.r * coef)),
		g: Math.min(255, Math.round(color.g * coef)),
		b: Math.min(255, Math.round(color.b * coef)),
	}
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function makeColorMapFromTrio(paletteIndex) {
	var color1 = PALETTES[paletteIndex + 0];
	var color2 = PALETTES[paletteIndex + 1];
	var color3 = PALETTES[paletteIndex + 2];

	var light = luminance(color3, 2.40);
	var dark1 = luminance(color1, 0.45);
	var dark2 = luminance(color2, 0.45);
	var dark3 = luminance(color3, 0.45);
	var dark4 = luminance(light,  0.45);

	return [color1, color2, color3, light, dark1, dark2, dark3, dark4];
}


//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Game.prototype.changeColors = function (paletteIndex) {
	var newSheet = color.swapPalette(assets.tilesheet, {
		source: COLOR_SOURCE_MAP, 
		target: makeColorMapFromTrio(paletteIndex * 3)
	});
	tilesheet(newSheet.canvas);
	this.grid.map.redraw();
	this.grid.nextLine.redraw();
};

Game.prototype.addPause = function (pause) {
	this.pause += pause;
	this.maxPause = this.pause;
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
Game.prototype._setEvents = function () {
	var self = this;
	this.grid.on('destroyed', function (nDestroyed, chain) {
		var nChain = chain.length;
		var points;
		if (nChain > 1) {
			points = nDestroyed * nChain;
			sfx('combo', 1, 0, 2 * (nChain - 2));
		} else {
			points = nDestroyed - 2;
			sfx('destroy');
		}

		self.score += points;

		// TODO: balance the pause formula
		var pause = points * 60;
		self.addPause(pause);
	});
};