var viewManager       = require('../viewManager');
var BlockifyAnimation = require('./BlockifyAnimation');

var currentAnimation = null;

exports.open = function (game) {

	// TODO screen shake
	var blockifyAnimation = new BlockifyAnimation(game);
	// TODO wall up animation
	// TODO dithering animation

	blockifyAnimation.on('end', function () {
		viewManager.open('result', game); // TODO -> next animation -> result screen
	});


	currentAnimation = blockifyAnimation;
};

exports.update = function (gamepad) {
	currentAnimation.update();
};