var constants    = require('../constants');
var EventEmitter = require('EventEmitter');
var TILE_WIDTH   = settings.tileSize.width;
var TILE_HEIGHT  = settings.tileSize.height;
var ANIM_SPEED   = 5;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function BlockifyAnimation(game) {
	EventEmitter.call(this);
	this.map    = game.grid.map;
	this.scroll = game.scroll;
	this.frame  = 0;
	this.line   = 0;
}
inherits(BlockifyAnimation, EventEmitter);
module.exports = BlockifyAnimation;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
BlockifyAnimation.prototype.update = function () {
	this.frame += 1;
	if (this.frame < ANIM_SPEED) return;

	this.frame = 0;

	var map = this.map;

	if (this.line >= map.height) {
		getMap('blockLine').draw(0, constants.GRID_HEIGHT * constants.TILE_SIZE - this.scroll)
		this.emit('end');
		return;
	}

	for (var i = 0; i < map.width; i++) {
		if (map.get(i, this.line).sprite) {
			map.set(i, this.line, 56);
		}
	}

	sfx('blockify');
	draw(map, 0, -this.scroll);

	this.line += 1;
};
