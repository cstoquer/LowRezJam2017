var viewManager = require('../viewManager');

function printNumber(n, x, y) {
	n = n.toString();
	var len = n.length;
	for (var i = 0; i < len; i++) {
		var c = ~~n[i];
		sprite(198 + c, x + (i - len / 2) * 6, y);
	}
}

var currentGame;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
exports.open = function (game) {
	currentGame = game;
	camera(1, 1);
	var background = getMap('result');
	background.redraw(); // switch to current palette
	draw(background);
	printNumber(game.stage + 1, 33, 31);
	printNumber(game.score,     33, 49);
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
exports.update = function (gamepad) {
	if (btnp.A || btnp.B || gamepad.btnp.A || gamepad.btnp.B) viewManager.open('hiscore', currentGame);
};