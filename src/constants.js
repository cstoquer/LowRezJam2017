exports.TILE_SIZE        = settings.tileSize.width;
exports.GRID_WIDTH       = 6;
exports.GRID_HEIGHT      = 10;
exports.DARK_TILE_OFFSET = 16; // sprite id offset in the tilesheet to get to the "dark" tiles
exports.SWITCHING_SPRITE = 16; // sprite used for switching tile
exports.MAX_PALETTE      = 25;