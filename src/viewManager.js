var getAnyGamepad = require('gamepad').getAnyGamepad;
var views         = {};
var currentView   = null;

exports.add = function (id, view) {
	views[id] = view;
};


exports.update = function () {
	var gamepad = getAnyGamepad();
	currentView.update(gamepad);
};

exports.open = function (id, params) {
	currentView = views[id];
	currentView && currentView.open && currentView.open(params);
};
