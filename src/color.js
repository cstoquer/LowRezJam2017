var Texture = require('Texture');

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function RgbColor(r, g, b) {
	this.r = r;
	this.g = g;
	this.b = b;
}

function HslColor(h, s, l) {
	this.h = h;
	this.s = s;
	this.l = l;
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function hueToRgb(p, q, t) {
	if (t < 0) t += 1;
	if (t > 1) t -= 1;
	if (t < 1 / 6) return p + (q - p) * 6 * t;
	if (t < 3 / 6) return q;
	if (t < 4 / 6) return p + (q - p) * (4 / 6 - t) * 6;
	return Math.min(255, Math.round(p * 255));
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
/**
 * Converts an HSL color value to RGB. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes h, s, and l are contained in the set [0, 1]
 *
 * @param   {number}  h - hue
 * @param   {number}  s - saturation
 * @param   {number}  l - lightness
 * @return  {object} - {r, g, b} color object
 */
function hslToRgb(color) {
	var h = color.h;
	var s = color.s;
	var l = color.l;

	var r, g, b;

	if (s === 0) {
		r = g = b = l; // achromatic
	} else {
		var t = 1 / 3;
		var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
		var p = 2 * l - q;
		r = hueToRgb(p, q, h + t);
		g = hueToRgb(p, q, h);
		b = hueToRgb(p, q, h - t);
	}

	return new RgbColor(r, g, b);
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
/**
 * Converts an RGB color value to HSL. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes r, g, and b are contained in the set [0, 255] and
 * returns h, s, and l in the set [0, 1].
 *
 * @param   {number}  color.r       The red color value
 * @param   {number}  color.g       The green color value
 * @param   {number}  color.b       The blue color value
 * @return  {array}           The HSL representation
 */
function rgbToHsl(color) {
	var r = color.r / 255;
	var g = color.g / 255;
	var b = color.b / 255;

	var max = Math.max(r, g, b), min = Math.min(r, g, b);
	var h, s, l = (max + min) / 2;

	if (max == min) {
		h = s = 0; // achromatic
	} else {
		var d = max - min;
		s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
		switch(max){
			case r: h = (g - b) / d + (g < b ? 6 : 0); break;
			case g: h = (b - r) / d + 2; break;
			case b: h = (r - g) / d + 4; break;
		}
		h /= 6;
	}

	return new HslColor(h, s, l);
}

exports.rgbToHsl = rgbToHsl;
exports.hslToRgb = hslToRgb;

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
function getImagePixels(image) {
	// make a copy of the image in a new texture
	var texture = new Texture(image.width, image.height);
	texture.draw(image, 0, 0);

	var ctx = texture.ctx;

	// get pixel data
	var imageData = ctx.getImageData(0, 0, image.width, image.height);
	var pixels = imageData.data;
	return pixels;
}

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
/**
 * @param {Image | Texture} image - the image to swap palette
 * @param {object} paletteSwap - palette swap data
 *        {object}   paletteSwap.source - source color, key is a color code, value is the index in paletteSwap.target
 *        {object[]} paletteSwap.target - target color, RGB values of target palette
 */
exports.swapPalette = function (image, paletteSwap) {
	// make a copy of the image in a new texture
	var texture = new Texture(image.width, image.height);
	texture.draw(image, 0, 0);

	var ctx = texture.ctx;

	// get pixel data
	var imageData = ctx.getImageData(0, 0, image.width, image.height);
	var pixels = imageData.data;

	// change pixel colors
	for (var i = 0; i < pixels.length; i += 4) {
		var a = pixels[i + 3];
		if (a === 0) continue;

		var r = pixels[i    ];
		var g = pixels[i + 1];
		var b = pixels[i + 2];

		var key = r + ':' + g + ':' + b;
		var targetIndex = paletteSwap.source[key];
		if (targetIndex === undefined) {
			continue;
		}

		var target = paletteSwap.target[targetIndex];

		pixels[i    ] = target.r;
		pixels[i + 1] = target.g;
		pixels[i + 2] = target.b;
	}

	// put back pixels data
	ctx.putImageData(imageData, 0, 0);

	return texture;
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
exports.getPaletteMapFromImage = function (image) {
	var pixels  = getImagePixels(image);
	var palette = {};
	var index   = 0;

	for (var i = 0; i < pixels.length; i += 4) {
		var r = pixels[i    ];
		var g = pixels[i + 1];
		var b = pixels[i + 2];
		var key = r + ':' + g + ':' + b;
		palette[key] = index++;
	}

	return palette;
};

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
exports.getColorsFromImage = function (image) {
	var pixels = getImagePixels(image);
	var colors = [];
	for (var i = 0; i < pixels.length; i += 4) {
		var r = pixels[i    ];
		var g = pixels[i + 1];
		var b = pixels[i + 2];
		var color = new RgbColor(r, g, b);
		colors.push(color);
	}
	return colors;
};