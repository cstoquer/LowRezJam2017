var viewManager = require('./viewManager');
var Game        = require('./gameView/Game');

viewManager.add('splash',    require('./splashView'));
viewManager.add('game over', require('./gameOverView'));
viewManager.add('result',    require('./resultView'));
viewManager.add('hiscore',   require('./hiscoreView'));
viewManager.add('game',      new Game());


var loopData = {
	start: 0.025,
	end:  -0.033
};

audioManager.playLoopSound('sfx', 'bgm', 0.6, 0, 0, loopData.start, loopData.end);


viewManager.open('splash', 0);

//▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
// Update is called once per frame
exports.update = viewManager.update;
