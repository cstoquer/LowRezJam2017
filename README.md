# Trisk
A game made during the LowRezJam 2017

- [demo on GitLab Pages](https://cstoquer.gitlab.io/LowRezJam2017/)
- [The game on itch.io](https://pixwlk.itch.io/trisk)
